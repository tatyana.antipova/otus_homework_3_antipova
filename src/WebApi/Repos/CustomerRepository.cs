﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WebApi.Abstractions;
using WebApi.Exceptions;
using WebApi.Models;

namespace WebApi.Repos
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataContext _dbContext;
        public CustomerRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<long> CreateCustomerAsync(Customer customer)
        {
            if (customer is null)
                throw new ArgumentException(nameof(customer));
                      
            if(await GetCustomerFromDb(customer.Id) is not null)
                throw new CustomerAlreadyExistException(customer.Id);

            var item = await _dbContext.Customers.AddAsync(customer);
            await _dbContext.SaveChangesAsync();

            return item.Entity.Id;
            
        }

        public async Task<Customer> GetCustomerAsync(long id)
        {
            return await GetCustomerFromDb(id) ?? throw new CustomerNotFoundException(id);
        }

        private async Task<Customer> GetCustomerFromDb(long id)
        {
            return await _dbContext.Customers
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
