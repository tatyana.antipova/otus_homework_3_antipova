﻿using System;
using System.Threading.Tasks;
using WebClient.Commands;

namespace WebClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            Invoker invoker = new Invoker();

            while (true)
            {
                Console.WriteLine(await invoker.GetCommand().Execute());
                var input = Console.ReadLine();

                try
                {
                    var command = invoker.GetCommand(input!);
                    Console.WriteLine(await command.Execute());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }
            }
        }
    }
}