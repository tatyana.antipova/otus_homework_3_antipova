using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Abstractions;
using WebApi.Exceptions;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _repository;
        public CustomerController(ICustomerRepository repository)
        {
            _repository = repository;
        }
        [HttpGet("{id:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(CustomerNotFoundException), StatusCodes.Status404NotFound)]
        public async Task<Customer> GetCustomerAsync([FromRoute] long id)
        {
            var result = await _repository.GetCustomerAsync(id);
            return result;
        }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(CustomerAlreadyExistException), StatusCodes.Status409Conflict)]
        public async Task<long> CreateCustomerAsync([FromBody] Customer customer)
        {
            var result = await _repository.CreateCustomerAsync(customer);
            return result;
        }
    }
}