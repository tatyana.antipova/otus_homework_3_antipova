﻿using Microsoft.EntityFrameworkCore;

namespace WebApi.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        {
            Database.EnsureCreated();
        }


        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.UseIdentityColumns();

            builder.HasDefaultSchema("public");

            builder.Entity<Customer>().ToTable("customers");

            builder.Entity<Customer>().HasKey(x => x.Id);

            builder.Entity<Customer>().Property(x => x.Id)
                .ValueGeneratedOnAdd()
                .IsRequired();

            base.OnModelCreating(builder);
        }
    }
}
