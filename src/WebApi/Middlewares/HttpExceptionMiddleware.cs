﻿using Microsoft.AspNetCore.Http;
using System.Text.Json;
using System.Threading.Tasks;
using WebApi.Exceptions;
using System.Net.Mime;
using System.Text;
using System;

namespace WebApi.Middlewares
{
    public class HttpExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public HttpExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this.next.Invoke(context);
            }
            catch (HttpException httpException)
            {
                context.Response.Clear();
                context.Response.StatusCode = httpException.StatusCode;
                context.Response.ContentType = MediaTypeNames.Application.Json;
                await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(
                       JsonSerializer.Serialize(new
                       {
                           Message = httpException.Message,
                       })));
            }
            catch (Exception ex)
            {
                context.Response.Clear();
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                context.Response.ContentType = MediaTypeNames.Application.Json;
                await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(
                       JsonSerializer.Serialize(new
                       {
                           Message = ex.Message,
                       })));
            }
        }
    }
}
