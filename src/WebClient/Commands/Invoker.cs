﻿using System;
using WebClient.Abstractions;

namespace WebClient.Commands
{
    internal class Invoker
    {
        public ICommand GetCommand()
        {
            return new StartCommand();
        }
        public ICommand GetCommand(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return new StartCommand();

            var parts = input.Split(' ');

            ICommand operation = parts[0] switch
            {
                StartCommand.Name => new StartCommand(),
                SetRandomCustomerCommand.Name => new SetRandomCustomerCommand(),
                GetCustomerCommand.Name => new GetCustomerCommand(string.Join(' ', parts[1..])),
                _ => throw new NotImplementedException()
            };

            return operation;
        }
    }
}
