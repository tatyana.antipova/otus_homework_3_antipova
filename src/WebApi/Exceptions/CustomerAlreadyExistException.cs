﻿using Microsoft.AspNetCore.Http;

namespace WebApi.Exceptions
{
    public class CustomerAlreadyExistException : HttpException
    {
        private const string MESSAGE = "Пользователь с Id {0} уже существует в базе";

        public CustomerAlreadyExistException(long id)
            : base(
                  httpStatusCode: StatusCodes.Status409Conflict,
                  message: string.Format(MESSAGE, id))
        {
        }
    }
}
