﻿using System;
using System.Threading.Tasks;
using WebClient.Abstractions;

namespace WebClient.Commands
{
    internal class StartCommand : ICommand
    {
        public const string Name = "Start";

        public Task<string> Execute()
        {
            return Task.FromResult(@"Доступные команды:
    Start
    GetCustomer ид
    SetRandomCustomer
");
        }

    }
}
