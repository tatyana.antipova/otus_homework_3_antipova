﻿using System.Threading.Tasks;

namespace WebClient.Abstractions
{
    public interface ICommand
    {
        Task<string> Execute();
    }
}
