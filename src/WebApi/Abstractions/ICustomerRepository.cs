﻿using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Abstractions
{
    public interface ICustomerRepository
    {
        Task<Customer> GetCustomerAsync(long id);

        Task<long> CreateCustomerAsync(Customer customer);
    }
}
