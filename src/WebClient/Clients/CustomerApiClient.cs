﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Net.Http.Json;

namespace WebClient.Services
{
    internal static class CustomerApiClient
    {
        private const string URI_BASE = "http://localhost:4300";
        public static async Task<Customer> GetCustomerAsync(long id)
        {
            using var client = new HttpClient();

            client.BaseAddress = new Uri(URI_BASE);
            
            client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

            var url = $"customers/{id}";
            HttpResponseMessage response = await client.GetAsync(url);
            
            response.EnsureSuccessStatusCode();
            var customer = await response.Content.ReadFromJsonAsync<Customer>();

            return customer;
        }

        public static async Task<long> SetCustomerAsync(Customer customer)
        {
            using var client = new HttpClient();

            client.BaseAddress = new Uri(URI_BASE);
            
            client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

            var url = $"customers";

            var json = JsonSerializer.Serialize(customer);

            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, data);
            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return long.Parse(result);
        }
    }
}
