﻿using Microsoft.AspNetCore.Http;

namespace WebApi.Exceptions
{
    public class CustomerNotFoundException : HttpException
    {
        private const string MESSAGE = "Пользователь с Id {0} не найден в базе";

        public CustomerNotFoundException(long id)
            : base(
                  httpStatusCode: StatusCodes.Status404NotFound,
                  message: string.Format(MESSAGE, id))
        {
        }
    }
}
