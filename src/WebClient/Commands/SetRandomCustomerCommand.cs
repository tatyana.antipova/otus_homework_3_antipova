﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WebClient.Abstractions;
using WebClient.Services;

namespace WebClient.Commands
{
    internal class SetRandomCustomerCommand : ICommand
    {
        public const string Name = "SetRandomCustomer";

        public async Task<string> Execute()
        {
            var rand = new Random();
            Customer generatedCustomer = new Customer()
            { 
                Firstname = RandomString(rand, rand.Next(2, 10)),
                Lastname = RandomString(rand, rand.Next(2, 10)),
            };
            var customerId = await CustomerApiClient.SetCustomerAsync(generatedCustomer);

            var createdCustomer = await CustomerApiClient.GetCustomerAsync(customerId);

            return $@"
Customer добавлен:
Id: {createdCustomer.Id}
Firstname: {createdCustomer.Firstname}
Lastname: {createdCustomer.Lastname}
";
        }

        public static string RandomString(Random random, int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
