﻿using System;
using System.Threading.Tasks;
using WebClient.Abstractions;
using WebClient.Services;

namespace WebClient.Commands
{
    internal class GetCustomerCommand : ICommand
    {
        public const string Name = "GetCustomer";
        private readonly long _id;
        public GetCustomerCommand(string id)
        {
            if (long.TryParse(id, out long result) && result > 0)
            {
                _id = result;
            }
            else
            {
                throw new ArgumentException("Ид пользователя должен быть целым положительным числом.");
            }
        }
        public async Task<string> Execute()
        {
            var customer = await CustomerApiClient.GetCustomerAsync(_id);

            return $@"
Id: {customer.Id}
Firstname: {customer.Firstname}
Lastname: {customer.Lastname}
";
        }

    }
}
